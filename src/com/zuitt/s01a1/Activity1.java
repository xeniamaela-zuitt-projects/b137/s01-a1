package com.zuitt.s01a1;

public class Activity1 {
    public static void main(String[] args) {
        String firstName = "Jane";
        String lastName = "Doe";
        double english = 97.5;
        double math = 85.2;
        double science = 93;
        double average = (english + math + science)/3;
        System.out.println(firstName + " " + lastName + "'s average grade is " + average);
    }
}
